"""
Executable loader.

Author: Vlad Topan (vtopan/gmail)
"""
import json
import os
import re
import struct

import pefile

from libbinview import BinView
from libmisc import PROJ_PATH
from libmsg import log, err, dbg


API_MS_WIN_MAGIC = 'api_ms_win_all'
MOCK_LIBS = ['advapi32.dll', 'certcli.dll', 'comctl32.dll', 'comdlg32.dll', 'crypt32.dll',
        'cryptdll.dll', 'gdi32.dll', 'iphlpapi.dll', 'kernel32.dll', 'msimg32.dll',
        'normaliz.dll', 'ntdll.dll', 'odbcconf.dll', 'ole32.dll', 'oleaut32.dll',
        'rpcrt4.dll', 'shell32.dll', 'shfolder.dll', 'shlwapi.dll', 'urlmon.dll',
        'user32.dll', 'version.dll', 'wininet.dll', 'wldap32.dll', 'ws2_32.dll', 'wsock32.dll',
        API_MS_WIN_MAGIC, 'msvcrt.dll', 'msvcp.dll',
        ]
MOCK_EXP_BY_ORD = 4
ALWAYS_IMPORT = {
    'kernel32.dll': ['LoadLibraryA', 'LoadLibraryW', 'GetProcAddress', 'VirtualAlloc',
        'GetModuleHandleA', 'GetProcAddress', 'GetLastError', 'ExitProcess'],
    'ntdll.dll': ['RtlZeroMemory'],
    'user32.dll': ['GetMessageW'],
    }
RX = {
    'msvcrt.dll': (r'(^api-ms-win-crt-[a-z]+-l\d+-\d+-\d+|msvcrt?\d+|vcruntime\d+)\.dll$', re.I),
    'msvcp.dll': (r'^msvcp\d+\.dll$', re.I),
    API_MS_WIN_MAGIC: (r'^api-ms-win-(\w+-)+l\d+-\d+-\d+\.dll$', re.I),
}
for k in RX:
    RX[k] = re.compile(*RX[k])


class Loader:
    """
    Executable image loader.

    :param mwemu: MwEmu instance.
    """


    def __init__(self, mock_unknown_libs=False):
        self.lib_ext = 'dll'    # if self.env.mode == 'win', else ...
        self.images = {}
        self.img_by_name = {}
        self.lib_info = {}
        self.known_exports = {}
        self.main_img = None
        self.img_sizes = {}
        self.init_done = 0
        self.available_hooks = {}
        self.mock_unknown_libs = mock_unknown_libs
        self._mock_dll = None
        self.all_syms = None


    def load(self, filename=None, data=None, img=None, main=False, functions=None):
        """
        Load an executable image.

        :param main: If True, this is the main image (from which execution starts).
        :param functions: Functions to mock (dict mapping function name to arg count).
        """
        if not img:
            img = ExeImage(filename, data, ldr=self)
        if img.imagebase in self.images:
            raise ValueError(f'ImageBase 0x{img.imagebase:x} is not free!')
        self.images[img.imagebase] = img
        self.img_by_name[img.libname] = img
        data = img.mem_view()
        self.img_sizes[img.imagebase] = len(data)
        self.mem.set_mem(img.imagebase, data=data, size=img.imgsize, name=f'Im[{img.libname}]')
        if main:
            self.env.insert_lib(img)
            self.cpu.set_ip(img.entrypoint)
            libs = img.imports()
            self.load_library('ntdll.dll')
            self.load_library('kernel32.dll')
            for lib, funs in libs.items():
                self.load_library(lib, functions=funs)
            img.resolve_imports(self.known_exports)
            self.main_img = img
            self.env.set_main(img)
        else:
            functions = sorted(set(list(functions or []) + ALWAYS_IMPORT.get(img.libname, [])))
            if functions:
                for fun in functions:
                    self.resolve_export(img.libname, fun)
        dbg(f'Image {img.libname} loaded @ 0x{img.imagebase:x}')
        return img


    def load_library(self, name, functions=None):
        """
        Load a library on demand.
        """
        img = None
        name = name.lower()
        norm_name = self.norm_lib_name(name)
        if name in self.img_by_name:
            img = self.img_by_name[name]
            if functions:
                for fun in functions:
                    self.resolve_export(img.libname, fun)
        elif norm_name in MOCK_LIBS or self.mock_unknown_libs:
            img = MockLib(name, self)
            self.load(img=img, functions=functions)
            self.env.insert_lib(img)
        else:
            raise ValueError(f'Unknown/unmockable library {name}!')
        return img.imagebase if img else None


    def resolve_export(self, lib, function):
        """
        Ensure the export exists in the given lib and return its address.

        :param lib: Name or ImageBase.
        """
        if type(lib) is int:
            if lib not in self.images:
                err(f'Cannot resolve {function} - no library @ 0x{lib:X}!')
                return
            img = self.images[lib]
            lib = img.libname
        else:
            lib = lib.lower()
            if lib not in self.img_by_name:
                err(f'Cannot resolve {function} - unknown library {lib}!')  # todo: try to load it?
                return
            img = self.img_by_name[lib]
        exports = img.exports()
        if function not in exports:
            if img.norm_name == API_MS_WIN_MAGIC:
                impl_lib = self.find_symbol(function)
                if impl_lib:
                    dbg(f'Resolving {img.libname}:{function} using {impl_lib}...', 10)
                    libinfo = self.get_lib_info(impl_lib)
                    funinfo = libinfo['exports'][function]
                    addr = self.resolve_export(impl_lib, function)
                    img.add_export(function, len(funinfo['args']) if funinfo['call'] == 'std' else 0, addr=addr)
                else:
                    err(f'MS-API function {function} in {img.libname} is unknown!')
                    return
            else:
                libinfo = self.get_lib_info(img.libname)
                if function in libinfo['exports']:
                    funinfo = libinfo['exports'][function]
                elif img.norm_name in ('msvcrt.dll', 'msvcp.dll'):
                    funinfo = {'call': 'cdecl', 'args': []}
                else:
                    err(f'Function {function} in {img.libname} is unknown!')
                    return
                img.add_export(function, len(funinfo['args']) if funinfo['call'] == 'std' else 0)
            addr = img.exports()[function]
            if img.libname not in self.known_exports:
                self.known_exports[img.libname] = {}
            self.known_exports[img.libname][function] = addr
            if self.cpu.log_apis and img.norm_name != API_MS_WIN_MAGIC:
                self.cpu.hook_api(lib, function, self.cpu.api_log_hook, (lib, function),
                        format_args=True)
            # check if hook exists
            norm_lib = self.norm_lib_name(lib)
            sym = f'{norm_lib}:{function}'
            if lib != norm_lib:
                self.mem.add_sym_alias(sym, f'{lib}:{function}')
            if sym in self.available_hooks:
                for hookfun in self.available_hooks[sym]:
                    self.cpu.hook_api(lib, function, hookfun, self.mwemu)
        else:
            addr = exports[function]
        return addr


    def get_lib_info(self, name):
        """
        Returns information (imagebase, exported functions) about the given lib name.

        See tools/genwinapiinfo.py for adding information about more libs.
        """
        if name not in self.lib_info:
            res = {}
            norm_name = self.norm_lib_name(name)
            base = rf'{PROJ_PATH}/data/{self.cpu.arch}/{norm_name}.json'
            for filename in (base, base + '.override'):
                exists = os.path.isfile(filename)
                if exists:
                    d = json.load(open(filename))
                    for k, v in d.items():
                        if k not in res or type(v) is not dict:
                            res[k] = v
                        else:
                            res[k].update(v)
                dbg(f'API info file {filename} {"loaded" if exists else "missing"} (lib: {name})', 2)
            if not res:
                res = {'exports': {}}
                err(f'Warning: unknown library {name}! (see tools/genwinapiinfo.py)')
            self.lib_info[name] = res
        return self.lib_info[name]


    def norm_lib_name(self, name):
        """
        Returns a "normalized" library name (used to handle 'api-ms-win-*.dll').
        """
        for k in ('msvcrt.dll', 'msvcp.dll', API_MS_WIN_MAGIC):
            if RX[k].search(name):
                return k
        return name.lower()


    def get_mock_dll(self):
        """
        Returns the raw mock dll template.
        """
        if not self._mock_dll:
            self._mock_dll = open(f'{PROJ_PATH}/template-dll/template{self.cpu.bits}.dll',
                    'rb').read()
        return self._mock_dll


    def dump_image(self, filename, addr=None):
        """
        Dump executable image to disk.

        :param addr: ImageBase to dump from (if None, dump main image).
        """
        if addr is None:
            addr = self.main_img.imagebase
        img = self.images[addr]
        data = img.dump()
        open(filename, 'wb').write(data)


    def find_symbol(self, symbol):
        """
        Attempts to find the lib name for the given symbol.
        """
        if self.all_syms is None:
            self.all_syms = json.load(open(rf'{PROJ_PATH}/data/{self.cpu.arch}_symbols.json'))
        return self.all_syms.get(symbol)



class ExeImage:
    """
    Executable image abstraction.
    """


    def __init__(self, filename=None, libname=None, data=None, ldr=None):
        self.ldr = ldr
        if filename:
            self.filename = filename
        self.libname = libname or os.path.basename(filename).lower()
        pe = self.pe = pefile.PE(filename, data=data, fast_load=True)
        dbg(f'Loading PE {filename} (machine:{pefile.MACHINE_TYPE[pe.FILE_HEADER.Machine].rsplit("_")[-1]})...')
        self.load()


    def load(self):
        """
        Load image from disk.
        """
        pe = self.pe
        pe.parse_data_directories(directories=[0, 1, 9])    # export, import, tls
        self.imagebase = pe.OPTIONAL_HEADER.ImageBase
        self.entrypoint = pe.OPTIONAL_HEADER.AddressOfEntryPoint + self.imagebase
        self.imgsize = pe.OPTIONAL_HEADER.SizeOfImage
        self.num_sections = len(pe.sections)
        self.is_lib = bool(pe.FILE_HEADER.Characteristics & 0x2000)
        self._exports = None
        self.bits = 64 if pe.PE_TYPE == pefile.OPTIONAL_HEADER_MAGIC_PE_PLUS else 32
        self.ord2name = {}
        self.original_sections = [{'rva': sec.VirtualAddress, 'vsize': sec.Misc_VirtualSize,
            'offs': sec.PointerToRawData, 'rsize': sec.SizeOfRawData} for sec in self.pe.sections]
        self.header_size = pe.DOS_HEADER.e_lfanew + pe.FILE_HEADER.SizeOfOptionalHeader + 24 \
                + self.num_sections * 40
        self.original_header = pe.__data__[:self.header_size]
        # check for TLS callbacks
        self.tls_callbacks = []
        if hasattr(pe, 'DIRECTORY_ENTRY_TLS') \
                and pe.DIRECTORY_ENTRY_TLS.struct.AddressOfCallBacks:
            va = pe.DIRECTORY_ENTRY_TLS.struct.AddressOfCallBacks
            rva = va - self.imagebase
            fmt, size = ('I', 4) if self.bits == 32 else ('Q', 8)
            for i in range(100):
                cbk_va = struct.unpack('<' + fmt, pe.get_data(rva + size * i, size))[0]
                if not cbk_va:
                    break
                self.tls_callbacks.append(cbk_va)
            log(f'Image @ 0x{self.imagebase:X} has {len(self.tls_callbacks)} TLS callbacks '
                    f'(list @ 0x{va:X})')
        if self.ldr:
            self.data = self.ldr.mem.get_mem_view(self.imagebase)
            self.norm_name = self.ldr.norm_lib_name(self.libname)


    def sec_info(self, section):
        """
        Returns the name, virtual address and size of a section.
        """
        sec = self.pe.sections[section]
        va = sec.VirtualAddress + self.imagebase
        return (sec.Name, va, sec.Misc_VirtualSize)


    def sec_raw_data(self, section):
        """
        Returns the raw (file) data of a section.
        """
        return self.pe.sections[section].get_data()


    def mem_view(self):
        """
        Return the executable image data to be written to memory.
        """
        return self.pe.get_memory_mapped_image()


    def exports(self):
        """
        Returns a dict mapping export names to addresses.
        """
        if self._exports is None:
            self._exports = {}
            self.pe.parse_data_directories(directories=[0])
            for exp in self.pe.DIRECTORY_ENTRY_EXPORT.symbols:
                if exp.name:
                    self._exports[exp.name.decode('ascii', errors='replace')] = self.imagebase \
                        + exp.address
        return self._exports


    def imports(self):
        """
        Returns a dict mapping statically imported libs to lists of functions.
        """
        res = {}
        for imp in self.pe.DIRECTORY_ENTRY_IMPORT:
            name = imp.dll.decode('ascii', errors='replace').lower()
            if name not in res:
                res[name] = []
            for fun in imp.imports:
                if fun.name:
                    fun_name = fun.name.decode('ascii', errors='replace')
                else:
                    fun_name = f'ordinal_{fun.ordinal}'
                res[name].append(fun_name)
                if fun.ordinal:
                    self.ord2name[(name, fun.ordinal)] = fun_name
        return res


    def resolve_imports(self, symmap):
        """
        Resolve the imports of the given executable image.

        :param symmap: dict mapping library names to dicts mapping function names to VAs.
        """
        pe = self.pe
        ordinal_flag = 2 ** (self.bits - 1)
        dbg(f'IAT area @ 0x{self.imagebase + pe.OPTIONAL_HEADER.DATA_DIRECTORY[1].VirtualAddress:x}')
        hardcoded = {
            ('msvcrt.dll', '_acmdln'): self.ldr.env.cmdline_ptr_va,
            ('msvcrt.dll', '_wcmdln'): self.ldr.env.cmdlinew_ptr_va,
            ('msvcrt.dll', '__p__commode'): 0,     # fixme
            }
        for iid in pe.DIRECTORY_ENTRY_IMPORT:
            dll_name = iid.dll.decode('ascii', errors='replace').lower()
            ilt_rva = iid.struct.OriginalFirstThunk
            ilt = pe.get_import_table(ilt_rva)
            iat_rva = iid.struct.FirstThunk
            iat = pe.get_import_table(iat_rva)
            if iat is None:
                err(f'Failed parsing IAT @ RVA 0x{iid.struct.FirstThunk:x}!\n')
                continue
            if not ilt:
                ilt, ilt_rva = iat, iat_rva
            for idx in range(len(ilt)):
                hint_rva = ilt[idx].AddressOfData
                if hint_rva:
                    fun_name = None
                    if hint_rva & ordinal_flag:
                        ordinal = hint_rva & 0xFFFF
                        if (dll_name, ordinal) in self.ord2name:
                            fun_name = self.ord2name[(dll_name, ordinal)]
                        else:
                            fun_name = f'ordinal_{ordinal}'
                        dbg(f'Resolving imported ordinal {ordinal} from {dll_name} ({fun_name})...',
                                20)
                    else:
                        # hint = pe.get_word_from_data(pe.get_data(hint_rva, 2), 0)
                        fun_name = pe.get_string_at_rva(ilt[idx].AddressOfData + 2,
                                pefile.MAX_IMPORT_NAME_LENGTH)
                        dbg(f'Resolving imported function {fun_name} from {dll_name}...', 50)
                        if not pefile.is_valid_function_name(fun_name):
                            err(f'[!] Invalid imported function name {fun_name}!\n')
                            continue
                        fun_name = fun_name.decode('ascii', errors='replace')
                    imp_va = None
                    norm_dll_name = self.ldr.norm_lib_name(dll_name)
                    try:
                        imp_va = hardcoded.get((norm_dll_name, fun_name),
                                symmap.get(dll_name, {})[fun_name])
                    except KeyError:
                        # function is unknown (not present in API .jsons)
                        if norm_dll_name == API_MS_WIN_MAGIC:
                            for k, v in self.ldr.known_exports.items():
                                if fun_name in v:
                                    imp_va = v[fun_name]
                                    log(f'Note: using {fun_name} from {k} to emulate it in {dll_name}')
                                    break
                    if imp_va is None:
                        err(f'Cannot emulate unknown imported function {dll_name}:{fun_name} - THIS WILL CRASH IF CALLED!')
                    rva = pe.get_rva_from_offset(iat[idx].get_field_absolute_offset('AddressOfData'))
                    if self.ldr and fun_name:
                        self.ldr.mem.add_symbol(f'IAT<{dll_name}:{fun_name}>', self.imagebase + rva)
                    dbg(f'Patching @ {self.imagebase + rva:x} value {imp_va}...', 50)
                    self.data[rva:rva + self.ldr.cpu.word] = imp_va or 0xBADF000D   # 0xBADF000D means unknown API
                else:
                    err(f'AddressOfData @ index {idx} in table @ RVA {ilt_rva} is empty!\n')


    def dump(self):
        """
        Create a dump of the image from memory.
        """
        falign = self.pe.OPTIONAL_HEADER.FileAlignment
        hdr_size = (self.header_size + falign - 1) // falign * falign
        mem = self.ldr.mem
        hdr = BinView(self.original_header + b'\0' * (hdr_size - self.header_size))
        sec_table_offs = self.header_size - self.num_sections * 40
        data = []
        offs = hdr_size
        for i, sec in enumerate(self.original_sections):
            sec_data = mem.read(self.imagebase + sec['rva'], sec['vsize']).rstrip(b'\0')
            sec_size = (len(sec_data) + falign - 1) // falign * falign
            sec_data += (sec_size - len(sec_data)) * b'\0'
            data.append(sec_data)
            sec_offs = sec_table_offs + i * 40
            hdr.set_dw(sec_offs + 0x10, sec_size)
            hdr.set_dw(sec_offs + 0x14, offs)
            offs += sec_size
        data.insert(0, hdr)
        return b''.join(data)



class MockLib(ExeImage):
    """
    Mock library with fake exports.
    """

    def __init__(self, libname, ldr):
        self.ldr = ldr
        self.libname = libname.lower()
        self.imagebase = self.ldr.mem.find_mem_gap(0x40000)
        self.pe = pefile.PE(data=self.ldr.get_mock_dll())
        self.pe.OPTIONAL_HEADER.ImageBase = self.imagebase
        self.load()
        # read & save original exports
        self.pe.parse_data_directories(directories=[0])
        self.byarg_exports = {}
        for exp in self.pe.DIRECTORY_ENTRY_EXPORT.symbols:
            if self.bits == 64:
                if exp.name == b'fun':
                    self.byarg_exports = {i:exp.address for i in range(20)}
                    break
                continue
            num = exp.name[3:-3].decode('ascii')
            if not num.isnumeric():
                continue
            argcnt = int(num)    # funNarg function names
            self.byarg_exports[argcnt] = exp.address
        self.expdir_rva = self.pe.OPTIONAL_HEADER.DATA_DIRECTORY[0].VirtualAddress
        self.data = self.ldr.mem.get_mem_view(self.imagebase)
        self.reset_expdir_done = False


    def add_export(self, fun, argcnt, addr=None):
        """
        Add an exported function.

        :param addr: If given, point the export entry to this address.
        """
        if not self.reset_expdir_done:
            self.reset_expdir()
        exp_cnt = self.data.get_dw(self.expdir_rva + 0x14)
        name_cnt = self.data.get_dw(self.expdir_rva + 0x18)
        # function address
        if addr:
            # use given
            gate_rva = addr - self.imagebase
            if gate_rva < 0:
                gate_rva = 0x100000000 + gate_rva
        else:
            # patch in jump gate
            fun_rva = self.byarg_exports[argcnt]
            gate_rva = self.gate_rva
            self.ldr.mem.add_symbol(f'{self.libname}:{fun}', self.imagebase + gate_rva)
            patch_size = 11 if self.bits == 32 else 5
            delta = fun_rva - (gate_rva + patch_size)
            patch = b'\xE9' + struct.pack('<l', delta)
            if self.bits == 32:
                patch = b'\x8B\xFF\x55\x8B\xEC\x5D' + patch
            self.data.write(gate_rva, patch)
            addr = self.imagebase + gate_rva
            self.gate_rva += len(patch)
        self.ldr.mem.add_symbol(f'EAT<{fun}>', self.imagebase + self.aof_rva + exp_cnt * 4)
        self.data.set_dw(self.aof_rva + exp_cnt * 4, gate_rva)
        self._exports[fun] = addr
        # function name
        name_rva = self.crt_name_rva
        self.crt_name_rva += len(fun) + 1
        self.data[name_rva:name_rva + len(fun) + 1] = fun.encode('ascii') + b'\0'
        # function name address & ordinal
        self.data.set_dw(self.aon_rva + name_cnt * 4, name_rva)
        self.data.set_w(self.aono_rva + name_cnt * 2, exp_cnt)
        dbg(f'Added export {fun} to {self.libname} @ {addr:08X}')
        # counts
        self.data.set_dw(self.expdir_rva + 0x14, exp_cnt + 1)    # NumberOfFunctions
        self.data.set_dw(self.expdir_rva + 0x18, name_cnt + 1)   # NumberOfNames


    def reset_expdir(self):
        """
        Reset (clear) the export directory and move tables around to fit more data.
        """
        # expect section total size of 0x20000 bytes, the DD is at the very beginning
        dbg(f'Resetting export dir of {self.libname}...')
        self.data.set_dw(self.expdir_rva + 0x18, 0)         # NumberOfNames
        self.aof_rva = self.expdir_rva + 0x100
        self.ldr.mem.add_symbol(f'EAT:{self.libname}', self.imagebase + self.aof_rva)
        self.data.set_dw(self.expdir_rva + 0x1C, self.aof_rva)      # AddressOfFunctions
        self.aon_rva = self.expdir_rva + 0x4000
        self.ldr.mem.add_symbol(f'ExpNames:{self.libname}', self.imagebase + self.aon_rva)
        self.data.set_dw(self.expdir_rva + 0x20, self.aon_rva)      # AddressOfNames
        self.aono_rva = self.expdir_rva + 0x8000
        self.ldr.mem.add_symbol(f'ExpNameOrds:{self.libname}', self.imagebase + self.aono_rva)
        self.data.set_dw(self.expdir_rva + 0x24, self.aono_rva)     # AddressOfNameOrdinals
        self.crt_name_rva = self.expdir_rva + 0x10000
        self.gate_rva = 0x1200  # assumes .text @ 0x1000, allows 0x200 for original mock DLL code
        # export MOCK_EXP_BY_ORD functions by ordinal
        self.data.set_dw(self.expdir_rva + 0x14, MOCK_EXP_BY_ORD)  # NumberOfFunctions
        for i in range(MOCK_EXP_BY_ORD):
            fun_rva = self.byarg_exports[i + 1]
            self.data.set_dw(self.aof_rva + i * 4, fun_rva)
        # DLL name
        name_rva = self.expdir_rva + 0x30
        self.data.set_dw(self.expdir_rva + 0xC, name_rva)
        self.data.write(name_rva, self.libname.encode('ascii') + b'\0')
        self._exports = {}
        self.reset_expdir_done = 1



