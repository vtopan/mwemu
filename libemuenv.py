"""
Environment emulator.

Author: Vlad Topan (vtopan/gmail)
"""
import array
import binascii
import json
import os
import struct
import time

from libbinview import BinView
from libconst import CRT, Win, REG_KEY_MAP, X86
from libmsg import log, err, dbg


GDT_ADDR = 0xF0F80000
PEB_ADDR = 0xFD0000
PEB_SIZE = 0x200
PEB_LDR_ADDR = PEB_ADDR + PEB_SIZE
PEB_LDR_SIZE = 0xE00        # PEB + PEB_LDR total size must round up to 0x1000
TEB_ADDR = PEB_LDR_ADDR + PEB_LDR_SIZE
TEB_SIZE = 0x2000
WIN_STACK_START = 0xEC0000
WIN_STACK_SIZE = 0x100000   # 1 MB
PID = 2069
TID_START = 20066
H_STDIN = 0x94
H_STDOUT = 0x98
H_STDERR = 0x9C
H_INVALID = 0xFFFFFFFF
DEF_HWND = 0x69F0F0
BASE_SOCKET = 0x6950C0
REG_BASE = 0x69E000
WINDIR = 'c:\\windows'
BASE_MUTEX = 0x69B000
ENTRYPOINT_MAGIC = 0xF0F0E0E1


class EnvEmu:
    """
    Environment emulator (for Windows: stack, heap, SEH, PEB, TEB, some Windows APIs, etc.)

    :param mode: Environment to emulate ('win' only).
    :param winver: Windows version (as string or tuple).
    :param winsp: Service Pack (int or None).
    """

    def __init__(self, mode='win', winver='10', winsp=None, registry_source=None):
        self.mode = mode
        self.pid = PID
        self.tid_crt = TID_START
        self.sys_start_time = int(time.time()) - 6 * 3600
        self.winver = winver
        self.winsp = None
        self.threads = {}       # maps thread_id to (tid, stack_addr, stack_size, teb_addr, idx)
        self.desktop = r'Winsta0\Default'
        self.window = {'x': 100, 'xsize': 800, 'y': 100, 'ysize': 600, 'cols': 80, 'rows': 25,
                'title': 'Administrator: cmd.exe', 'hwnd': DEF_HWND}
        self.string_va = {'utf8':{}, 'utf16':{}}
        self.handles = {
            H_INVALID: {'filename': '<invalid-handle>', 'type': 2},
            H_STDIN: {'filename':'<stdin>', 'type': 2},
            H_STDOUT: {'filename':'<stdout>', 'type': 2},
            H_STDERR: {'filename':'<stderr>', 'type': 2}
            }
        self.sockets = {}
        self.wsa_last_error = 0
        self.win_errormode = 0
        self.regkeys = {}
        self._registry = None
        self.registry_source = registry_source
        self.paths = {'windows': WINDIR}
        self.libs = []
        self.mutexes = {}
        self.entrypoints = []
        self.crt_ep = 0
        self.unhandled_exc_filter = None
        if mode == 'win':
            self.stacks_crt = WIN_STACK_START
        else:
            raise ValueError(f'Mode {mode} is invalid / unknown!')


    def init(self):
        """
        Set up memory (GDT, CS/DS/ES/SS, FS/GS, etc.)
        """
        self.init_paths()
        # first set up the main thread's stack space (to get a valid ESP)
        self.main_tid = self.new_thread()
        # environment variables
        self.env_va = self.env_crt_va = self.mem.init_mem('Env', 0x10000)
        self.empty_str_va = self.env_str_va(b'\0\0', 'empty-string')[0]  # both A and W
        self.cmdline_va = self.env_str_va(b'\0' * 512, name='cmdline')[0]
        self.cmdline_ptr_va = self.env_str_va(struct.pack('<L', self.cmdline_va),
                name='cmdline-ptr')[0]
        self.cmdlinew_va = self.env_str_va(b'\0\0' * 512, enc='W', name='cmdlinew')[0]
        self.cmdlinew_ptr_va = self.env_str_va(struct.pack('<L', self.cmdlinew_va),
                name='cmdlinew-ptr')[0]
        self.mem.init_mem('Page0', 0x1000, 0, prot=0)
        # version
        if self.mode == 'win':
            if self.winver in ('8', '10'):
                # todo: check for manifest, report real version if present
                self.os_ver = {'major': 6, 'minor': 2, 'build': 9200}
                self.os_sp = self.winsp
                self.os_suite_mask = 0x00000110     # VER_SUITE_TERMINAL | VER_SUITE_SINGLEUSERTS
                self.os_prod_type = 1               # VER_NT_WORKSTATION
            else:
                raise NotImplementedError('Unknown Windows version {self.winver}!')
        self.init_peb_teb()
        self.init_gdt()
        self.init_crt()
        dbg('Environment initialized', 5)


    def set_main(self, img):
        """
        Set main image.
        """
        cmdline = self.cmdline = b'c:\\' + os.path.basename(self.ldr.main_img.filename).encode(
            'ascii', errors='replace') + b'\0'
        self.mem.write(self.cmdline_va, cmdline)
        cmdlinew = self.cmdlinew = ('c:\\' + os.path.basename(self.ldr.main_img.filename)
            + '\0').encode('utf16')[2:]
        self.mem.write(self.cmdlinew_va, cmdlinew)
        # RTL_USER_PROCESS_PARAMETERS (ImagePathName & CommandLine)
        self.rupp = b'\0' * 56 + struct.pack('<HHL', len(self.cmdlinew), len(self.cmdlinew),
                self.cmdlinew_va) * 2
        self.rupp_va = self.env_crt_va
        self.mem.write(self.rupp_va, self.rupp)
        self.env_crt_va += len(self.rupp)
        # environment variables
        self.env_vars = [('PATH', 'c:\\windows')]   # todo: add more environment variables
        addrs, waddrs = [], []
        for k, v in self.env_vars:
            addrs.append(self.env_str_va(f'{k}={v}')[0])
            waddrs.append(self.env_str_va(f'{k}={v}', enc='W')[0])
        addrs.append(0)
        waddrs.append(0)
        env_varsa = struct.pack('<' + 'I' * len(addrs), *addrs)
        self.env_vars_va = self.env_str_va(env_varsa, name='env-vars')[0]
        env_varsw = struct.pack('<' + 'I' * len(waddrs), *waddrs)
        self.env_varsw_va = self.env_str_va(env_varsw, name='env-varsw')[0]
        # STARTUPINFO (A/W)
        if self.cpu.bits == 32:
            pat, size = '<LLLLLLLLLLLLHHLLLL', 68
        else:
            pat, size = '<LxxxxQQQLLLLLLLLHHxxxxQQQQ', 104
        for enc, name in [
                ('A', 'startupinfoa'),
                ('W', 'startupinfow'),
                ]:
            desktop_va = self.env_str_va(self.desktop, enc=enc)[0]
            title_va = self.env_str_va(self.window['title'], enc=enc)[0]
            value = struct.pack(pat, size, self.empty_str_va, desktop_va, title_va,
                self.window['x'], self.window['y'], self.window['xsize'], self.window['ysize'],
                self.window['cols'], self.window['rows'], 0, 0, 10, 0, 0,
                H_STDIN, H_STDOUT, H_STDERR)
            if size != len(value):
                raise ValueError(f'STARTUPINFO{enc} has size {len(value)}, should be {size}!')
            setattr(self, name, value)
            self.env_str_va(value, name=name.upper())
        # PEB fields
        if self.cpu.bits == 32:
            self.peb.set_dw(0x8, self.ldr.main_img.imagebase)
            self.peb.set_dw(0x10, self.rupp_va)
        else:
            self.peb.set_qw(0x10, self.ldr.main_img.imagebase)
            self.peb.set_dw(0x20, self.rupp_va)
        # argv
        self.run_args = [self.ldr.main_img.filename]
        args = []
        for e in self.run_args:
            args.append(self.env_str_va(e)[0])
        args.append(0)
        argv = struct.pack('<' + 'I' * len(args), *args)
        self.argv_va = self.env_str_va(argv, name='argv')[0]
        self.prepare_entrypoints(img)


    def env_str_va(self, value, enc='A', name=None):
        """
        Get a string's encoded addres (add if necessary).

        :return: (address, encoded_data)
        """
        enc = 'utf8' if enc in ('A', 'utf8') else 'utf16'
        if value not in self.string_va[enc]:
            if type(value) is not bytes:
                if not value.endswith('\0'):
                    value += '\0'
                buf = value.encode(enc, errors='replace')
                if buf.startswith(b'\xFF\xFE'):
                    buf = buf[2:]
            else:
                buf = value
            va = self.new_env_block(data=buf)
            self.string_va[enc][value] = (va, buf)
            if name:
                self.string_va[enc][name] = (va, buf)
            self.mem.add_symbol(f'EnvStr({enc}):' + (name if name else repr(buf)), va)
        return self.string_va[enc][value]


    def new_env_block(self, size=None, data=None):
        """
        Allocates a new memory block for environment data.
        """
        if not size:
            size = len(data)
        va = (self.env_crt_va + 7) & ~7   # 8-byte aligned
        self.env_crt_va = va + ((size + 7) & ~7)
        if data:
            self.mem.write(va, data)
        return va


    def new_unicode_string(self, string, addr=None):
        """
        Allocates & initializes a UNICODE_STRING structure.

        :return: The VA address.
        """
        string_va, encoded = self.env_str_va(value=string, enc='W')
        pat = '<HHL' if self.cpu.bits == 32 else '<HHxxxxQ'
        usdata = struct.pack(pat, len(string) * 2, len(encoded), string_va)
        if not addr:
            addr = self.new_env_block(data=usdata)
        else:
            self.mem.write(addr, usdata)
        self.mem.add_symbol(f'US[{len(string) * 2}]:{string}', addr)
        return addr


    def handle_by_filename(self, filename):
        """
        Get a handle by filename (e.g. '<stdin>').
        """
        if filename in (-10, -11, -12):
            # standard windows handles
            filename = ('<stderr>', '<stdout>', '<stdin>')[filename + 12]
        res = [k for k, v in self.handles.items() if v['filename'] == filename]
        return res[0] if res else 0xFFFFFFFF    # INVALID_HANDLE_VALUE


    def init_gdt(self):
        """
        Initialize the GDT.
        """
        zero_desc = struct.pack('<Q', 0)
        cs_desc = X86.make_gdt_desc(0, 0xFFFFF, X86.make_gdt_type(True))
        dbg(f'CS descriptor: {binascii.hexlify(cs_desc)}', 10)
        ds_desc = X86.make_gdt_desc(0, 0xFFFFF, X86.make_gdt_type(False))
        dbg(f'DS descriptor: {binascii.hexlify(ds_desc)}', 10)
        teb_desc = X86.make_gdt_desc(TEB_ADDR, 4, X86.make_gdt_type(False))   # 4 pages
        dbg(f'FS/GS descriptor: {binascii.hexlify(teb_desc)}', 10)
        ss_desc = X86.make_gdt_desc(0, 0xFFFFF, X86.make_gdt_type(False), DPL=0)
        dbg(f'SS descriptor: {binascii.hexlify(ss_desc)}', 10)
        gdt = zero_desc + cs_desc + ds_desc + teb_desc + ss_desc + 11 * zero_desc
        self.mem.set_mem(GDT_ADDR, size=0x1000, data=gdt, name='GDT')
        self.cpu.set_reg('GDTR', (0, GDT_ADDR, len(gdt) - 1, 0))
        self.cpu.set_reg('CS', X86.make_seg_sel(1))
        ds_sel = X86.make_seg_sel(2)
        self.cpu.set_reg('DS', ds_sel)
        self.cpu.set_reg('ES', ds_sel)
        self.cpu.set_reg('FS', X86.make_seg_sel(3) if self.cpu.bits == 32 else ds_sel)
        self.cpu.set_reg('GS', ds_sel)
        self.cpu.set_reg('SS', X86.make_seg_sel(4, RPL=0))
        if self.cpu.bits == 64:
            self.cpu.set_msr(X86.MSR_GS_BASE, TEB_ADDR)
        self.cpu.set_sp(self.stack_top)


    def init_peb_teb(self):
        """
        Initialize the PEB and TEB.
        """
        self.peb_addr = PEB_ADDR
        self.peb_ldr_addr = PEB_LDR_ADDR
        peb = BinView(PEB_SIZE)
        if self.cpu.bits == 32:
            peb.set_dw(0xC, self.peb_ldr_addr)
            peb.set_dw(0xA4, self.os_ver['major'])
            peb.set_dw(0xA8, self.os_ver['minor'])
            peb.set_w(0xAC, self.os_ver['build'])
        else:
            peb.set_qw(0x18, self.peb_ldr_addr)
            peb.set_dw(0x118, self.os_ver['major'])
            peb.set_dw(0x11C, self.os_ver['minor'])
            peb.set_w(0x120, self.os_ver['build'])
        peb_ldr = BinView(PEB_LDR_SIZE)
        peb_ldr.set_dw(0x4, 1)      # initialized
        if self.cpu.bits == 32:
            peb_ldr.set_dw(0, 0x30)     # size
            peb_ldr.set_dw(0xC, self.peb_ldr_addr + 0xC)          # load order->Flink
            peb_ldr.set_dw(0x10, self.peb_ldr_addr + 0xC)         # load order->Blink
            peb_ldr.set_dw(0x14, self.peb_ldr_addr + 0x14)        # mem order->Flink
            peb_ldr.set_dw(0x18, self.peb_ldr_addr + 0x14)        # mem order->Blink
            peb_ldr.set_dw(0x1C, self.peb_ldr_addr + 0x1C)        # init order->Flink
            peb_ldr.set_dw(0x20, self.peb_ldr_addr + 0x1C)        # init order->Blink
        else:
            peb_ldr.set_dw(0, 0x58)     # size
            peb_ldr.set_qw(0x10, self.peb_ldr_addr + 0x10)        # load order->Flink
            peb_ldr.set_qw(0x18, self.peb_ldr_addr + 0x10)        # load order->Blink
            peb_ldr.set_qw(0x20, self.peb_ldr_addr + 0x20)        # mem order->Flink
            peb_ldr.set_qw(0x28, self.peb_ldr_addr + 0x20)        # mem order->Blink
            peb_ldr.set_qw(0x30, self.peb_ldr_addr + 0x30)        # init order->Flink
            peb_ldr.set_qw(0x38, self.peb_ldr_addr + 0x30)        # init order->Blink
        self.mem.init_mem('PEB+LDR', PEB_SIZE + PEB_LDR_SIZE, self.peb_addr)
        self.mem.set_mem(self.peb_addr, data=peb)
        self.mem.set_mem(self.peb_ldr_addr, data=peb_ldr)
        self.peb = self.mem.get_mem_view(self.peb_addr, PEB_SIZE)
        self.peb_ldr = self.mem.get_mem_view(self.peb_ldr_addr, PEB_LDR_SIZE)
        teb = BinView(TEB_SIZE)
        self.teb_addr = TEB_ADDR
        # todo: SEH @ FS:[0], env. @ FS:[0x1C]
        tid, st_addr, st_size = self.threads[self.main_tid][:3]
        self.stack_top = st_addr + st_size
        if self.cpu.bits == 32:
            teb.set_dw(0x4, self.stack_top)                 # stack top
            teb.set_dw(0x8, st_addr)                        # stack bottom
            teb.set_dw(0x18, self.teb_addr)                 # self pointer
            teb.set_dw(0x20, self.pid)
            teb.set_dw(0x24, tid)
            teb.set_dw(0x30, self.peb_addr)
        else:
            teb.set_qw(0x8, self.stack_top)                 # stack top
            teb.set_qw(0x10, st_addr)                       # stack bottom
            teb.set_qw(0x30, self.teb_addr)                 # self pointer
            teb.set_qw(0x40, self.pid)
            teb.set_qw(0x48, tid)
            teb.set_qw(0x60, self.peb_addr)
        self.mem.set_mem(self.teb_addr, data=teb, name='TEB')
        self.fs = self.teb = self.mem.get_mem_view(self.teb_addr, TEB_SIZE)
        dbg(f'TEB @ 0x{self.teb_addr:x}, PEB @ 0x{self.peb_addr:x}')
        self.mem.add_symbol('TEB', self.teb_addr)
        self.mem.add_symbol('PEB', self.peb_addr)
        self.mem.add_symbol('PEB_LDR', self.peb_ldr_addr)


    def add_entrypoint(self, name, addr, args, index=None):
        """
        Add an entrypoint to the list (executed in order; pass index=0 to add to head).
        """
        if index is None:
            index = len(self.entrypoints)
        self.entrypoints.insert(index, (name, addr, args))


    def prepare_entrypoints(self, img):
        """
        Prepare the image entrypoints to be executed.
        """
        # reserve some stack space (presumably used by the loader)
        self.base_sp = self.cpu.get_sp() - 64
        if img.tls_callbacks:
            for i, addr in enumerate(img.tls_callbacks):
                # args: hDLL, reason (1=DLL_PROCESS_ATTACH), reserved
                self.add_entrypoint(f'TLSCallback#{i}@0x{addr:X}', addr, (img.imagebase, 1, 0))
        if img.is_lib:
            # args: hinstDLL, fdwReason (1=DLL_PROCESS_ATTACH), lpvReserved (1=static load)
            self.add_entrypoint(f'DLLEntry@0x{img.entrypoint:X}', img.entrypoint, (img.imagebase, 1,
                1))
        else:
            self.add_entrypoint(f'WinMain@0x{img.entrypoint:X}', img.entrypoint, tuple())
        self.mem.init_mem('EP-Hook', 0x1000, ENTRYPOINT_MAGIC & ~0xFFF)
        self.cpu.set_code_hook(self.next_entrypoint, addr=ENTRYPOINT_MAGIC)
        self.next_entrypoint()


    def next_entrypoint(self, ip=None, retaddr=None, args=None, user_data=None):
        """
        Prepare the next entrypoint in line for execution.

        Note: This function is also used as a code hook callback.
        """
        if self.crt_ep:
            prev_ep = self.entrypoints[self.crt_ep - 1]
            log(f'Entrypoint {prev_ep[0]} completed.')
        if self.crt_ep >= len(self.entrypoints):
            log('All entrypoints completed.')
            self.cpu.stop()
            return
        name, addr, args = self.entrypoints[self.crt_ep]
        self.cpu.set_ip(addr)
        self.cpu.set_sp(self.base_sp)
        # prepare parameters
        if len(args) < 4:
            args += (0,) * (4 - len(args))
        for i, val in enumerate(args[::-1]):
            self.cpu.push(val)
            if i < 4 and self.cpu.bits == 64:
                setattr(self.cpu, X86.X64_CALL_REG[i], val)
        # return address
        self.cpu.push(ENTRYPOINT_MAGIC)
        log(f'Entrypoint {name} prepared.')
        # stop current emulation (in cases this is a code hook callback)
        self.cpu.emu.emu_stop()
        self.crt_ep += 1


    def init_crt(self):
        """
        Init internal CRT stuff.
        """
        va = self.crt_vars_va = self.env_str_va(struct.pack('<LL', CRT.O_BINARY, CRT.IOCOMMIT),
                name='CRT-vars')[0]
        self.fmode_addr = va
        self.commode_addr = va + 4


    def init_paths(self):
        """
        Set up paths.
        """
        windir = self.paths['windows']
        # todo: syswow64
        self.paths['system'] = f'{windir}\\system32'
        self.paths['syswow64'] = f'{windir}\\syswow64'


    def set_peb_imagebase(self, imagebase):
        """
        Update ImageBase in PEB.
        """
        self.mem.set_mem(PEB_ADDR + 8, data=struct.pack('<L', imagebase))


    def new_stack(self, thread_id):
        """
        Returns a (addr, size) tuple for the new stack.
        """
        if self.mode == 'win':
            size = WIN_STACK_SIZE
            addr = self.stacks_crt
            self.stacks_crt -= size
        self.cpu.set_sp(addr + size)
        dbg(f'Initializing stack @ 0x{addr:x}[0x{size:x}] for thread {thread_id}...')
        self.mem.set_mem(addr, size=size, name=f'Stack/Th[{thread_id}]')
        return (addr, size)


    def new_tid(self):
        """
        Returns a new TID.
        """
        res = self.tid_crt
        self.tid_crt += 1
        return res


    def set_last_error(self, value):
        """
        Set the Windows API last error.
        """
        self.teb[0x34:0x38] = value


    def set_wsa_last_error(self, value):
        """
        Set the Windows API WSA last error.
        """
        self.wsa_last_error = value


    def prepare_exception(self, code, flags, addr, exc_args=0):
        """
        Prepare an EXCEPTION_POINTERS structure.
        """
        exc_rec = struct.pack('<LLLLL', code, flags, 0, addr, len(exc_args))
        if exc_args:
            exc_rec += array.array('L', exc_args).tobytes()
        self.last_exc_record = self.env_str_va(exc_rec, name='EXC_REC')[0]
        self.last_exc_pointers = self.env_str_va(struct.pack('<LL', self.last_exc_record, 0),
                name='EXC_PTRS')[0]     # todo: create an actual CONTEXT structure in arg#2
        return self.last_exc_pointers


    def new_thread(self):
        """
        Create a new thread (including stack, TEB, etc.).

        :return: TID.
        """
        # todo: teb
        idx = len(self.threads)
        tid = self.new_tid()
        st_addr, st_size = self.new_stack(tid)
        self.threads[tid] = (tid, st_addr, st_size, idx)
        return tid


    def new_socket(self, family, stype, protocol):
        """
        Creates a new socket.
        """
        s = len(self.sockets) + BASE_SOCKET
        self.sockets[s] = {'family': family, 'type': stype, 'protocol': protocol, 'connected': 0,
                'laddr': None, 'lport': None, 'raddr': None, 'rport': None}
        return s


    def init_registry(self, filename=None):
        """
        Load the emulated Windows registry from a file.
        """
        if not filename:
            if self.registry_source:
                filename = self.registry_source
        data = {k:{} for k in Win.__dict__ if k.startswith('HKEY_')}
        if filename:
            if filename.endswith('.json'):
                data.update(json.load(open(filename)))
            else:
                raise NotImplementedError('Unknown registry source filetype!')
        for k in data:
            handle = getattr(Win, k)
            self.regkeys[handle] = {'data': data[k], 'path': k, 'handle': handle}
        self._registry = data


    def winreg_open(self, path, parent=None, create=True):
        """
        Creates/opens a new registry key object.
        """
        if self._registry is None:
            self.init_registry()
        if not parent:
            if '\\' in path:
                parent, path = path.split('\\', 1)
                parent = getattr(Win, REG_KEY_MAP.get(parent, parent))
            else:
                parent, path = path, None
        if parent in self.regkeys:
            crt = self.regkeys[parent]['data']
            parent_str = self.regkeys[parent]['path']
        else:
            parent_str = Win.NAME_HKEY[parent]
            if parent_str not in self._registry:
                # unknown root key
                return None
            else:
                crt = self._registry[parent_str]
        crtpath = parent_str
        if not path:
            return parent
        for part in path.split('\\'):
            lpart = part.lower()
            if not part:
                continue
            if lpart not in crt:
                if create:
                    crt[lpart] = {}
                else:
                    # non-existent registry key
                    return None
            crt = crt[lpart]
            crtpath += '\\' + part
        handle = REG_BASE + len(self.regkeys)
        self.regkeys[handle] = {'data': crt, 'path': crtpath, 'handle': handle}
        return handle


    def new_mutex(self, name):
        """
        Create a new mutex object.
        """
        handle = BASE_MUTEX + len(self.mutexes)
        self.mutexes[name] = {'name': name, 'handle': handle}
        return handle


    def insert_lib(self, img):
        """
        Insert a loaded lib into the environment.

        This must be called in "LoadOrder".
        """
        self.libs.append(img)
        self.mem.add_symbol(f'IB:{img.libname}', img.imagebase)
        # create & populate the LDR_DATA_TABLE_ENTRY (aka LDR_MODULE) structure
        size = 104 if self.cpu.bits == 32 else 0x11D  # sizeof(LDR_DATA_TABLE_ENTRY)
        va = img.ldr_data_va = self.new_env_block(size)
        ldr = img.ldr_data = self.mem.get_mem_view(img.ldr_data_va, size=size)
        if self.cpu.bits == 32:
            ldr.set_dw(0x18, img.imagebase)
            ldr.set_dw(0x1C, img.entrypoint)
            ldr.set_dw(0x20, img.imgsize)
        else:
            ldr.set_qw(0x30, img.imagebase)
            ldr.set_qw(0x38, img.entrypoint)
            ldr.set_qw(0x40, img.imgsize)
        offs1, offs2 = (0x24, 0x2C) if self.cpu.bits == 32 else (0x48, 0x58)
        self.new_unicode_string(f'{self.paths["system"]}\\{img.libname}', addr=va + offs1)
        self.new_unicode_string(img.libname, addr=va + offs2)
        if self.cpu.bits == 32:
            ldr.set_w(0x34, 1)      # load count
        # load / memory / init order
        size = self.cpu.word
        if self.cpu.bits == 32:
            deltas = (0, 8, 16)
            flink_offs = 0xC
            blink_offs = 0x10
            this_blink_offs = 4
        else:
            deltas = (0, 0x10, 0x20)
            flink_offs = 0x10
            blink_offs = 0x18
            this_blink_offs = 8
        for delta in deltas:
            crt_va = img.ldr_data_va + delta
            old_last_va = self.peb_ldr.get_uint(blink_offs + delta, size)
            self.mem.set_uint(old_last_va, size, crt_va)                       # (old) last Flink -> this
            ldr.set_uint(delta, size, self.peb_ldr_addr + flink_offs + delta)  # this Flink -> PEB_LDR
            self.peb_ldr.set_uint(blink_offs + delta, size, crt_va)            # PEB_LDR's Blink -> this
            ldr.set_uint(this_blink_offs + delta, size, old_last_va)           # this Blink -> old last


