; Template DLL to be used in emulating Windows APIs
;
; Author: Vlad Topan (vtopan/gmail)

format PE64 GUI 4.0 DLL
entry DllMain

include 'win64a.inc'

section '.text' code readable executable

proc fun
    xor rax, rax
    xor rcx, rcx
    mov cl, 0x68
    mov [gs:rcx], rax  ; clear last error
    inc rax
    ret
endp

proc callimports
    call [SomeImport1]
    call [SomeImport2]
    call [SomeImport3]
    ret
endp

db 0x11000 dup(0x90)

proc DllMain hinstDLL, fdwReason, lpvReserved
mov rax, TRUE
ret
endp

.create_relocations:
repeat 0x400
mov rax, .create_relocations
end repeat

section '.idata' import data readable writeable

library\
kernel, 'someother.dll'

import kernel,\
SomeImport1, 'SomeImport1',\
SomeImport2, 'SomeImport2',\
SomeImport3, 'SomeImport3'

section '.data' data readable writeable
db 0x10000 dup(0)

section '.edata' export data readable

export 'paddingpaddingpaddingpaddingpadding.dll',\
fun, 'fun',\
callimports, 'callimports'

db 0x20000 dup(0)

data fixups
end data
