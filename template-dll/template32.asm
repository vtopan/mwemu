; Template DLL to be used in emulating Windows APIs
;
; Author: Vlad Topan (vtopan/gmail)

format PE GUI 4.0 DLL
entry DllMain

include 'win32a.inc'

section '.text' code readable executable

macro clerr_ret1
{
    mov [fs:0x34], dword 0  ; clear last error
    xor eax, eax
    inc eax
}

proc fun0arg stdcall
    clerr_ret1
    ret
endp

proc fun1arg stdcall arg1:DWORD
    clerr_ret1
    ret
endp

proc fun2arg stdcall arg1:DWORD, arg2:DWORD
    clerr_ret1
    ret
endp

proc fun3arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD
    clerr_ret1
    ret
endp

proc fun4arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD
    clerr_ret1
    ret
endp

proc fun5arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD
    clerr_ret1
    ret
endp

proc fun6arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD
    clerr_ret1
    ret
endp

proc fun7arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD
    clerr_ret1
    ret
endp

proc fun8arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD
    clerr_ret1
    ret
endp

proc fun9arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD, arg9:DWORD
    clerr_ret1
    ret
endp

proc fun10arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD, arg9:DWORD, arg10:DWORD
    clerr_ret1
    ret
endp

proc fun11arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD, arg9:DWORD, arg10:DWORD, arg11:DWORD
    clerr_ret1
    ret
endp

proc fun12arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD, arg9:DWORD, arg10:DWORD, arg11:DWORD, arg12:DWORD
    clerr_ret1
    ret
endp

proc fun13arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD, arg9:DWORD, arg10:DWORD, arg11:DWORD, arg12:DWORD, arg13:DWORD
    clerr_ret1
    ret
endp

proc fun14arg stdcall arg1:DWORD, arg2:DWORD, arg3:DWORD, arg4:DWORD, arg5:DWORD, arg6:DWORD, arg7:DWORD, arg8:DWORD, arg9:DWORD, arg10:DWORD, arg11:DWORD, arg12:DWORD, arg13:DWORD, arg14:DWORD
    clerr_ret1
    ret
endp

proc callimports stdcall
    call [SomeImport1]
    call [SomeImport2]
    call [SomeImport3]
    ret
endp

db 0x11000 dup(0x90)

proc DllMain hinstDLL, fdwReason, lpvReserved
mov eax, TRUE
ret
endp

.create_relocations:
repeat 0x400
mov eax, .create_relocations
end repeat

section '.idata' import data readable writeable

library\
kernel, 'someother.dll'

import kernel,\
SomeImport1, 'SomeImport1',\
SomeImport2, 'SomeImport2',\
SomeImport3, 'SomeImport3'

section '.data' data readable writeable
db 0x10000 dup(0)

section '.edata' export data readable

export 'paddingpaddingpaddingpaddingpadding.dll',\
fun0arg, 'fun0arg',\
fun1arg, 'fun1arg',\
fun2arg, 'fun2arg',\
fun3arg, 'fun3arg',\
fun4arg, 'fun4arg',\
fun5arg, 'fun5arg',\
fun6arg, 'fun6arg',\
fun7arg, 'fun7arg',\
fun8arg, 'fun8arg',\
fun9arg, 'fun9arg',\
fun10arg, 'fun10arg',\
fun11arg, 'fun11arg',\
fun12arg, 'fun12arg',\
fun13arg, 'fun13arg',\
fun14arg, 'fun14arg',\
callimports, 'callimports'

db 0x20000 dup(0)

data fixups
end data
